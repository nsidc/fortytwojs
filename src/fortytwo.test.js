var fortytwo = require('./index');

test('The answer to life, the universe, and everything is 42.', () => {
    expect(fortytwo()).toBe(42);
});
