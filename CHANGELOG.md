# v2.0.1 (5/11/2020)

* Update README with clearer workflow.
* Remove Yarn (just use npm).
* Switch to the MIT license.
