FortyTwoJS
---

An npm package to demonstrate how to test, build and publish to
the [NPM Registry](https://www.npmjs.com/)
using [CircleCI](https://circleci.com/).

[![CircleCI](https://circleci.com/bb/nsidc/fortytwojs.svg?style=svg)](https://circleci.com/bb/nsidc/fortytwojs)

[![npm version](https://badge.fury.io/js/fortytwojs.svg)](https://badge.fury.io/js/fortytwojs)

Prerequisites
---

* [NodeJS](https://nodejs.org/en/)

Development
---

Install dependencies (substitute 'npm' if you don't have yarn):

    $ npm install

Lint and run tests:

    $ npm run lint
    $ npm test

Workflow
---

TL;DR: Use
[GitHub Flow](https://guides.github.com/introduction/flow/index.html).

In more detail:

1. Create a feature branch.
2. Create and push commits on that branch.
3. The feature branch will get built on CircleCI with each push.
4. Update the CHANGELOG with description of changes.
5. Create a Pull Request on BitBucket.
6. When the feature PR is merged, master will get built on CircleCI.

Releasing
---

1. Update the CHANGELOG to list the new version.
2. Add files and commit

        $ git add CHANGELOG.md ...
        $ git commit -m "Release v.X.Y.Z"

3. Bump the version to the desired level:

        $ npm version (major|minor|patch)

4. Push

        $ git push origin master --tags

The new version will get built in CircleCI and pushed to
the [NPM Registry](https://www.npmjs.com/package/fortytwojs).

Installing
---
To install and use it in another project:

    $ npm install fortytwojs

License
---
This code is licensed and distributed under the terms of the MIT
License (see LICENSE).
